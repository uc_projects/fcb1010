#ifndef switches_h
#define switches_h

#define SWLED10 41
#define SWLED9 39
#define SWLED8 37
#define SWLED7 35
#define SWLED6 33
#define SWLED5 31
#define SWLED4 29
#define SWLED3 27
#define SWLED2 25
#define SWLED1 23

#define SW_1_7 32
#define SW_2_8 30
#define SW_3_9 28
#define SW_10_4 26
#define SW_up_5 24
#define SW_down_6 22
#define SW_common_upper 38
#define SW_common_lower 36

class Switches
{
  public:
    Switches();
    int readSwitches();
    void setup();
};

#endif
