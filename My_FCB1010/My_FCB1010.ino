#include <MIDI.h>
#include <midi_Defs.h>
#include <midi_Message.h>
#include <midi_Namespace.h>
#include <midi_Settings.h>

#include "sevenseg.h"
#include "switches.h"

Sevenseg sevenseg = Sevenseg();
Switches switches = Switches();

//#define SERIAL_DEBUG

#ifndef SERIAL_DEBUG
MIDI_CREATE_INSTANCE(HardwareSerial, Serial, MIDI);
#endif

//#include <AndroidAccessory.h>
//#include <stdio.h>
//#include <stdlib.h>
void HandleNoteOn(byte channel, byte pitch, byte velocity) { 

  // Do whatever you want when you receive a Note On.
  
  if (velocity == 0) {
    // This acts like a NoteOff.
  }

  // Try to keep your callbacks short (no delays ect) as the contrary would slow down the loop()
  // and have a bad impact on real-time performance.
}


void setup() {
  // initialize serial communication at 9600 bits per second:
  //

#ifndef SERIAL_DEBUG
  MIDI.begin(0);            	// Launch MIDI with default options
	// input channel is set to 0
  // Connect the HandleNoteOn function to the library, so it is called upon reception of a NoteOn.
  //MIDI.setHandleNoteOn(HandleNoteOn);  // Put only the name of the function
   MIDI.sendNoteOn(42,127,1);  // Send a Note (pitch 42, velo 127 on channel 1)
   delay(1);		// Wait for a second
   MIDI.sendNoteOff(42,0,1);   // Stop the note
#else
   Serial.begin(9600);
#endif

  sevenseg.setup();
  switches.setup();
  
  pinMode(SWLED10, OUTPUT); //10
  pinMode(SWLED9, OUTPUT); //9
  pinMode(SWLED8, OUTPUT);  //8
  pinMode(SWLED7, OUTPUT);  //7
  pinMode(SWLED6, OUTPUT);  //6  
  pinMode(SWLED5, OUTPUT);  //5
  pinMode(SWLED4, OUTPUT);  //4
  pinMode(SWLED3, OUTPUT);  //3
  pinMode(SWLED2, OUTPUT);  //2
  pinMode(SWLED1, OUTPUT);  //1

//all switch LED's off
  digitalWrite(SWLED10, HIGH); //10
  digitalWrite(SWLED9, HIGH); //9
  digitalWrite(SWLED8, HIGH);  //8
  digitalWrite(SWLED7, HIGH);  //7
  digitalWrite(SWLED6, HIGH);  //6  
  digitalWrite(SWLED5, HIGH);  //5
  digitalWrite(SWLED4, HIGH);  //4
  digitalWrite(SWLED3, HIGH);  //3
  digitalWrite(SWLED2, HIGH);  //2
  digitalWrite(SWLED1, HIGH);  //1
  
 
}


void control_change(int cc, int ccval){
#ifndef SERIAL_DEBUG
  MIDI.sendControlChange(cc, ccval, 1);
#else
  Serial.print("cc: ");
  Serial.print(cc, DEC);
  Serial.print(", val: ");
  Serial.print(ccval, DEC);
  Serial.print("\n");
#endif 
  sevenseg.decode7SEG(cc); 
}
/*
long map(long x, long in_min, long in_max, long out_min, long out_max){
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}*/

int CCOUNT = 0;
int exprA = 0;
int _exprA = 0;
int __exprA = 0;
boolean expAchanged = false;
boolean sendExpA(){
    // read the input on analog pin 0:
  int a = analogRead(A0);
  int vA = map(a, 455, 995, 0, 127);  
  int expAvgA = (exprA + _exprA + __exprA + vA)/4;
  if (vA == exprA) {
    expAchanged = false;
    return false;
  }
  __exprA = _exprA;
  _exprA = exprA;
  if (expAvgA == exprA) {
    expAchanged = false;
    return false;
  }
  exprA = expAvgA;
//  if(expressionA != _expressionA && ((expressionA > _expressionA+10 ) ||  (expressionA < _expressionA-10 ) )){

#ifdef SERIAL_DEBUG
  Serial.print("A0: ");
  Serial.print(a, DEC);
  Serial.print("\n");
#endif  
  control_change(12, expAvgA);     
  return true;
}

int exprB = 0;
int _exprB = 0;
int __exprB = 0;
boolean expBchanged = false;
boolean sendExpB(){
  int a = analogRead(A1);
  int vB = map(a, 39, 988, 0, 127);
  int expAvgB = (exprB + _exprB + __exprB + vB)/4;
  if (vB == exprB) {
    expBchanged = false;
    return false;
  }
  __exprB = _exprB;
  _exprB = exprB;
  if (expAvgB == exprB) {
    expBchanged = false;
    return false;
  }
  exprB = expAvgB;  
 // if(expressionB != _expressionB && ((expressionB > _expressionB+10 ) ||  (expressionA < _expressionB-10 ) )){

#ifdef SERIAL_DEBUG
  Serial.print("A1: ");
  Serial.print(a, DEC);
  Serial.print("\n");
#endif
  control_change(13, expAvgB);     
  return true;
}

bool SWLED11 = false;
bool SWLED12 = false;
void sendSwitches(){
  int v = 2;
	int swHold = switches.readSwitches();
    switch(swHold){
      case 1:
       v = digitalRead(SWLED1);
       if(v == 1){
         control_change(0, 0);
         digitalWrite(SWLED1, LOW);  //1
       } else {
         control_change(0, 255);
         digitalWrite(SWLED1, HIGH);  //1         
       }
       break;      
      case 2:
       v = digitalRead(SWLED2);
       if(v == 1){      
         control_change(1, 0);
         digitalWrite(SWLED2, LOW);  //2
       } else {
         control_change(1, 255);
         digitalWrite(SWLED2, HIGH);  //2
       }         
       break;      
      case 3:
       v = digitalRead(SWLED3);
       if(v == 1){      
         control_change(2, 0);
         digitalWrite(SWLED3, LOW);  //3
       } else {
         control_change(2, 255);
         digitalWrite(SWLED3, HIGH);  //3         
       }
       break;      
      case 4:
       v = digitalRead(SWLED4);
       if(v == 1){       
         control_change(3, 0);
         digitalWrite(SWLED4, LOW);  //4       
       } else {
         control_change(3, 255);
         digitalWrite(SWLED4, HIGH);  //4                
       }
       break;      
      case 5:
       v = digitalRead(SWLED5);
       if(v == 1){       
         control_change(4, 0);
         digitalWrite(SWLED5, LOW);  //5
       } else {
         control_change(4, 255);
         digitalWrite(SWLED5, HIGH);  //5         
       }
       break;      
      case 6:
       v = digitalRead(SWLED6);
       if(v == 1){      
         control_change(5, 0);
         digitalWrite(SWLED6, LOW);  //6  
       } else {
         control_change(5, 255);
         digitalWrite(SWLED6, HIGH);  //6           
       }
       break;      
      case 7:
       v = digitalRead(SWLED7);
       if(v == 1){
         control_change(6, 0);
         digitalWrite(SWLED7, LOW);  //7
       } else {
         control_change(6, 255);
         digitalWrite(SWLED7, HIGH);  //7         
       }
       break;      
      case 8:
       v = digitalRead(SWLED8);
       if(v == 1){
         control_change(7, 0);
         digitalWrite(SWLED8, LOW);  //8
       } else {
         control_change(7, 255);
         digitalWrite(SWLED8, HIGH);  //8         
       }
       break;      
      case 9:
       v = digitalRead(SWLED9);
       if(v == 1){
         control_change(8, 0);
         digitalWrite(SWLED9, LOW); //9
       } else  {
         control_change(8, 255);
         digitalWrite(SWLED9, HIGH); //9
       }         
       break;      
      case 10:
       v = digitalRead(SWLED10);
       if(v == 1){      
         control_change(9, 0);
         digitalWrite(SWLED10, LOW); //10
       } else {
         control_change(9, 255);
         digitalWrite(SWLED10, HIGH); //10
       }         
       break;      
      case 11:
		if(!SWLED11){
         control_change(10, 0);
		 SWLED11 = true;
        }else{
         control_change(10, 255);
		 SWLED11 = false;
		}
       break;      
      case 12:
		if(!SWLED12){ 
         control_change(11, 0);
		 SWLED12 = true;
        } else {
         control_change(11, 255);
		 SWLED12 = false;
		}
       break;
    } 
}

void test(){
  //decode7SEG(CCOUNT);
  //CCOUNT += 1;
  // print out the value you read:
  //Serial.println("ExpB" + expressionB);
  
   // MIDI.sendNoteOn(42,127,1);  // Send a Note (pitch 42, velo 127 on channel 1)
    //delay(200);		// Wait for a second
   // MIDI.sendNoteOff(42,0,1);   // Stop the note  
  // Call MIDI.read the fastest you can for real-time performance.
  //MIDI.read();   //will trigger CallBack on NoteOn
  /*if (MIDI.read()) {                    // Is there a MIDI message incoming ?
    switch(MIDI.getType()) {		// Get the type of the message we caught
      case ProgramChange:               // If it is a Program Change
		decode7SEG(MIDI.getData1());	// Blink the LED a number of times 
					// correponding to the program number 
					// (0 to 127, it can last a while..)
        break;
      // See the online reference for other message types
      default:
        break;
    }
  }*/
}
// the loop routine runs over and over again forever:
void loop() {
  sendExpA();
  sendExpB();
  sendSwitches();
  delay(30);        // delay in between reads for stability
}

