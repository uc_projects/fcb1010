
/********************************* 7Seg Display *******************************/
#include "Arduino.h"
#include "sevenseg.h"

Sevenseg::Sevenseg(){

}

void Sevenseg::setup(){
  //setup 7seg display
  pinMode(SEG7_A0, OUTPUT); //A
  pinMode(SEG7_B0, OUTPUT); //B
  pinMode(SEG7_C0, OUTPUT); //C
  pinMode(SEG7_D0, OUTPUT); //D
  pinMode(SEG7_E0, OUTPUT); //E
  pinMode(SEG7_F0, OUTPUT); //F
  pinMode(SEG7_G0, OUTPUT); //G
  
  pinMode(SEG7_A1, OUTPUT); //A
  pinMode(SEG7_B1, OUTPUT); //B
  pinMode(SEG7_C1, OUTPUT); //C
  pinMode(SEG7_D1, OUTPUT); //D
  pinMode(SEG7_E1, OUTPUT); //E
  pinMode(SEG7_F1, OUTPUT); //F
  pinMode(SEG7_G1, OUTPUT); //G
  all_segments_off();  
}

void Sevenseg::all_segments_off(){
  digitalWrite(SEG7_A0, HIGH); //A
  digitalWrite(SEG7_B0, HIGH); //B
  digitalWrite(SEG7_C0, HIGH); //C
  digitalWrite(SEG7_D0, HIGH); //D
  digitalWrite(SEG7_E0, HIGH); //E
  digitalWrite(SEG7_F0, HIGH); //F
  digitalWrite(SEG7_G0, HIGH); //G
  
  digitalWrite(SEG7_A1, HIGH); //A
  digitalWrite(SEG7_B1, HIGH); //B
  digitalWrite(SEG7_C1, HIGH); //C
  digitalWrite(SEG7_D1, HIGH); //D
  digitalWrite(SEG7_E1, HIGH); //E
  digitalWrite(SEG7_F1, HIGH); //F
  digitalWrite(SEG7_G1, HIGH); //G    
}
  
void Sevenseg::decode7SEG(short int val){
  short int lsb = (val & 15);  
  switch(lsb){
    case 0:
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, LOW); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, LOW); //D
      digitalWrite(SEG7_E0, LOW); //E
      digitalWrite(SEG7_F0, LOW); //F
      digitalWrite(SEG7_G0, HIGH); //G
      break;
    case 1:
      digitalWrite(SEG7_A0, HIGH); //A
      digitalWrite(SEG7_B0, LOW); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, HIGH); //D
      digitalWrite(SEG7_E0, HIGH); //E
      digitalWrite(SEG7_F0, HIGH); //F
      digitalWrite(SEG7_G0, HIGH); //G    
      break;
    case 2:
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, LOW); //B
      digitalWrite(SEG7_C0, HIGH); //C
      digitalWrite(SEG7_D0, LOW); //D
      digitalWrite(SEG7_E0, LOW); //E
      digitalWrite(SEG7_F0, HIGH); //F
      digitalWrite(SEG7_G0, LOW); //G    
      break;
    case 3:
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, LOW); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, LOW); //D
      digitalWrite(SEG7_E0, HIGH); //E
      digitalWrite(SEG7_F0, HIGH); //F
      digitalWrite(SEG7_G0, LOW); //G    
      break;
    case 4:
      digitalWrite(SEG7_A0, HIGH); //A
      digitalWrite(SEG7_B0, LOW); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, HIGH); //D
      digitalWrite(SEG7_E0, HIGH); //E
      digitalWrite(SEG7_F0, LOW); //F
      digitalWrite(SEG7_G0, LOW); //G    
      break;
    case 5:
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, HIGH); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, LOW); //D
      digitalWrite(SEG7_E0, HIGH); //E
      digitalWrite(SEG7_F0, LOW); //F
      digitalWrite(SEG7_G0, LOW); //G    
      break;
    case 6:
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, HIGH); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, LOW); //D
      digitalWrite(SEG7_E0, LOW); //E
      digitalWrite(SEG7_F0, LOW); //F
      digitalWrite(SEG7_G0, LOW); //G
      break;
    case 7:
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, LOW); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, HIGH); //D
      digitalWrite(SEG7_E0, HIGH); //E
      digitalWrite(SEG7_F0, HIGH); //F
      digitalWrite(SEG7_G0, HIGH); //G
      break;
    case 8:
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, LOW); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, LOW); //D
      digitalWrite(SEG7_E0, LOW); //E
      digitalWrite(SEG7_F0, LOW); //F
      digitalWrite(SEG7_G0, LOW); //G    
      break;
    case 9:
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, LOW); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, LOW); //D
      digitalWrite(SEG7_E0, HIGH); //E
      digitalWrite(SEG7_F0, LOW); //F
      digitalWrite(SEG7_G0, LOW); //G
      break;
    case 10: //A
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, LOW); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, HIGH); //D
      digitalWrite(SEG7_E0, LOW); //E
      digitalWrite(SEG7_F0, LOW); //F
      digitalWrite(SEG7_G0, LOW); //G    
      break;
    case 11: //b
      digitalWrite(SEG7_A0, HIGH); //A
      digitalWrite(SEG7_B0, HIGH); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, LOW); //D
      digitalWrite(SEG7_E0, LOW); //E
      digitalWrite(SEG7_F0, LOW); //F
      digitalWrite(SEG7_G0, LOW); //G    
      break;      
    case 12: //C
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, HIGH); //B
      digitalWrite(SEG7_C0, HIGH); //C
      digitalWrite(SEG7_D0, LOW); //D
      digitalWrite(SEG7_E0, LOW); //E
      digitalWrite(SEG7_F0, LOW); //F
      digitalWrite(SEG7_G0, HIGH); //G
      break;
    case 13: //d
      digitalWrite(SEG7_A0, HIGH); //A
      digitalWrite(SEG7_B0, LOW); //B
      digitalWrite(SEG7_C0, LOW); //C
      digitalWrite(SEG7_D0, LOW); //D
      digitalWrite(SEG7_E0, LOW); //E
      digitalWrite(SEG7_F0, HIGH); //F
      digitalWrite(SEG7_G0, LOW); //G   
      break;      
    case 14: //E
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, HIGH); //B
      digitalWrite(SEG7_C0, HIGH); //C
      digitalWrite(SEG7_D0, LOW); //D
      digitalWrite(SEG7_E0, LOW); //E
      digitalWrite(SEG7_F0, LOW); //F
      digitalWrite(SEG7_G0, LOW); //G    
      break;
    case 15: //F
      digitalWrite(SEG7_A0, LOW); //A
      digitalWrite(SEG7_B0, HIGH); //B
      digitalWrite(SEG7_C0, HIGH); //C
      digitalWrite(SEG7_D0, HIGH); //D
      digitalWrite(SEG7_E0, LOW); //E
      digitalWrite(SEG7_F0, LOW); //F
      digitalWrite(SEG7_G0, LOW); //G        
      break;      
  }    
  
  short int msb = (val & 240) / 16;
  switch(msb){
    case 0:
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, LOW); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, LOW); //D
      digitalWrite(SEG7_E1, LOW); //E
      digitalWrite(SEG7_F1, LOW); //F
      digitalWrite(SEG7_G1, HIGH); //G
      break;
    case 1:
      digitalWrite(SEG7_A1, HIGH); //A
      digitalWrite(SEG7_B1, LOW); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, HIGH); //D
      digitalWrite(SEG7_E1, HIGH); //E
      digitalWrite(SEG7_F1, HIGH); //F
      digitalWrite(SEG7_G1, HIGH); //G    
      break;
    case 2:
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, LOW); //B
      digitalWrite(SEG7_C1, HIGH); //C
      digitalWrite(SEG7_D1, LOW); //D
      digitalWrite(SEG7_E1, LOW); //E
      digitalWrite(SEG7_F1, HIGH); //F
      digitalWrite(SEG7_G1, LOW); //G    
      break;
    case 3:
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, LOW); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, LOW); //D
      digitalWrite(SEG7_E1, HIGH); //E
      digitalWrite(SEG7_F1, HIGH); //F
      digitalWrite(SEG7_G1, LOW); //G    
      break;
    case 4:
      digitalWrite(SEG7_A1, HIGH); //A
      digitalWrite(SEG7_B1, LOW); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, HIGH); //D
      digitalWrite(SEG7_E1, HIGH); //E
      digitalWrite(SEG7_F1, LOW); //F
      digitalWrite(SEG7_G1, LOW); //G    
      break;
    case 5:
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, HIGH); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, LOW); //D
      digitalWrite(SEG7_E1, HIGH); //E
      digitalWrite(SEG7_F1, LOW); //F
      digitalWrite(SEG7_G1, LOW); //G    
      break;
    case 6:
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, HIGH); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, LOW); //D
      digitalWrite(SEG7_E1, LOW); //E
      digitalWrite(SEG7_F1, LOW); //F
      digitalWrite(SEG7_G1, LOW); //G
      break;
    case 7:
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, LOW); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, HIGH); //D
      digitalWrite(SEG7_E1, HIGH); //E
      digitalWrite(SEG7_F1, HIGH); //F
      digitalWrite(SEG7_G1, HIGH); //G
      break;
    case 8:
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, LOW); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, LOW); //D
      digitalWrite(SEG7_E1, LOW); //E
      digitalWrite(SEG7_F1, LOW); //F
      digitalWrite(SEG7_G1, LOW); //G    
      break;
    case 9:
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, LOW); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, LOW); //D
      digitalWrite(SEG7_E1, HIGH); //E
      digitalWrite(SEG7_F1, LOW); //F
      digitalWrite(SEG7_G1, LOW); //G
      break;
    case 10: //A
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, LOW); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, HIGH); //D
      digitalWrite(SEG7_E1, LOW); //E
      digitalWrite(SEG7_F1, LOW); //F
      digitalWrite(SEG7_G1, LOW); //G    
      break;
    case 11: //b
      digitalWrite(SEG7_A1, HIGH); //A
      digitalWrite(SEG7_B1, HIGH); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, LOW); //D
      digitalWrite(SEG7_E1, LOW); //E
      digitalWrite(SEG7_F1, LOW); //F
      digitalWrite(SEG7_G1, LOW); //G    
      break;      
    case 12: //C
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, HIGH); //B
      digitalWrite(SEG7_C1, HIGH); //C
      digitalWrite(SEG7_D1, LOW); //D
      digitalWrite(SEG7_E1, LOW); //E
      digitalWrite(SEG7_F1, LOW); //F
      digitalWrite(SEG7_G1, HIGH); //G
      break;
    case 13: //d
      digitalWrite(SEG7_A1, HIGH); //A
      digitalWrite(SEG7_B1, LOW); //B
      digitalWrite(SEG7_C1, LOW); //C
      digitalWrite(SEG7_D1, LOW); //D
      digitalWrite(SEG7_E1, LOW); //E
      digitalWrite(SEG7_F1, HIGH); //F
      digitalWrite(SEG7_G1, LOW); //G   
      break;      
    case 14: //E
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, HIGH); //B
      digitalWrite(SEG7_C1, HIGH); //C
      digitalWrite(SEG7_D1, LOW); //D
      digitalWrite(SEG7_E1, LOW); //E
      digitalWrite(SEG7_F1, LOW); //F
      digitalWrite(SEG7_G1, LOW); //G    
      break;
    case 15: //F
      digitalWrite(SEG7_A1, LOW); //A
      digitalWrite(SEG7_B1, HIGH); //B
      digitalWrite(SEG7_C1, HIGH); //C
      digitalWrite(SEG7_D1, HIGH); //D
      digitalWrite(SEG7_E1, LOW); //E
      digitalWrite(SEG7_F1, LOW); //F
      digitalWrite(SEG7_G1, LOW); //G        
      break;      
  }  
}
