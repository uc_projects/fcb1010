
/********************* Switches **********************************************/

#include "Arduino.h"
#include "switches.h"

Switches::Switches(){

}

void Switches::setup(){
  pinMode(SW_1_7, OUTPUT);
  pinMode(SW_2_8, OUTPUT);
  pinMode(SW_3_9, OUTPUT);
  pinMode(SW_10_4, OUTPUT);
  pinMode(SW_up_5, OUTPUT);
  pinMode(SW_down_6, OUTPUT);
  pinMode(SW_common_upper, INPUT);
  pinMode(SW_common_lower, INPUT);
  
  digitalWrite(SW_1_7, HIGH);
  digitalWrite(SW_2_8, HIGH);
  digitalWrite(SW_3_9, HIGH);
  digitalWrite(SW_10_4, HIGH);
  digitalWrite(SW_up_5, HIGH);
  digitalWrite(SW_down_6, HIGH); 
}

int Switches::readSwitches(){
  int c_upper = digitalRead(SW_common_upper);
  int c_lower = digitalRead(SW_common_lower);
  int v = 2;
  if(c_upper == 1 || c_lower == 1){   
    int SwitchNum = 0;
    int c_upper = 0;
    int c_lower = 0;
    digitalWrite(SW_1_7, HIGH);
    digitalWrite(SW_2_8, LOW);
    digitalWrite(SW_3_9, LOW);
    digitalWrite(SW_10_4, LOW);
    digitalWrite(SW_up_5, LOW);
    digitalWrite(SW_down_6, LOW);   
    c_upper = digitalRead(SW_common_upper);
    c_lower = digitalRead(SW_common_lower);
    if(c_upper == 1){
      SwitchNum = 7;    
    }
    if(c_lower == 1){
      SwitchNum = 1;    
    }
    digitalWrite(SW_1_7, LOW);
    digitalWrite(SW_2_8, HIGH);
    c_upper = digitalRead(SW_common_upper);
    c_lower = digitalRead(SW_common_lower);
    if(c_upper == 1){
      SwitchNum = 8;
    }
    if(c_lower == 1){
      SwitchNum = 2;    
    }  
    digitalWrite(SW_2_8, LOW);
    digitalWrite(SW_3_9, HIGH);  
    c_upper = digitalRead(SW_common_upper);
    c_lower = digitalRead(SW_common_lower);
    if(c_upper == 1){
      SwitchNum = 9;
    }
    if(c_lower == 1){
      SwitchNum = 3;    
    }
    
    digitalWrite(SW_3_9, LOW);  
    digitalWrite(SW_10_4, HIGH);  
    c_upper = digitalRead(SW_common_upper);
    c_lower = digitalRead(SW_common_lower);
    if(c_upper == 1){
      SwitchNum = 10;
    }
    if(c_lower == 1){
      SwitchNum = 4;    
    }
  
    digitalWrite(SW_10_4, LOW);
    digitalWrite(SW_up_5, HIGH);  
    c_upper = digitalRead(SW_common_upper);
    c_lower = digitalRead(SW_common_lower);
    if(c_upper == 1){
      SwitchNum = 11;
    }
    if(c_lower == 1){
      SwitchNum = 5;    
    } 
  
    digitalWrite(SW_up_5, LOW);
    digitalWrite(SW_down_6, HIGH);   
    c_upper = digitalRead(SW_common_upper);
    c_lower = digitalRead(SW_common_lower);
    if(c_upper == 1){
      SwitchNum = 6;
    }
    if(c_lower == 1){
      SwitchNum = 12;    
    }
    
    digitalWrite(SW_1_7, HIGH);
    digitalWrite(SW_2_8, HIGH);
    digitalWrite(SW_3_9, HIGH);
    digitalWrite(SW_10_4, HIGH);
    digitalWrite(SW_up_5, HIGH);
    digitalWrite(SW_down_6, HIGH);
    while(digitalRead(SW_common_upper) == 1){
    }
    while(digitalRead(SW_common_lower) == 1){
    }  
    return SwitchNum;
  }
  return -1;
}
