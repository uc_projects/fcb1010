#ifndef sevenseg_h
#define sevenseg_h

#define SEG7_A0 13
#define SEG7_B0 12
#define SEG7_C0 11
#define SEG7_D0 10
#define SEG7_E0 9
#define SEG7_F0 8
#define SEG7_G0 7

#define SEG7_A1 15
#define SEG7_B1 16
#define SEG7_C1 17
#define SEG7_D1 18
#define SEG7_E1 19
#define SEG7_F1 20
#define SEG7_G1 21



class Sevenseg
{
  public:
    Sevenseg();
    void all_segments_off();
    void decode7SEG(short int val);
    void setup();
};

#endif
