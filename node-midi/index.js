var express = require('express');
var app = express();

var server_url = "127.0.0.1";
var server_port = 3001;
var io = require('socket.io').listen(3002);
var midi = require('midi');

if (typeof localStorage === "undefined" || localStorage === null) {
	var LocalStorage = require('node-localstorage').LocalStorage;
	localStorage = new LocalStorage('./localstorage', 500 * 1024 * 1024); //50mb limit
	//localStorage._deleteLocation('./localstorage');  //cleans up ./localstorage created during doctest
	//localStorage.setItem('myFirstKey', JSON.stringify({test: 123.0})); 
	//console.log(localStorage.getItem('myFirstKey'));
}

//app.use( express.json({limit: '50mb'}) );
app.use( express.urlencoded({
  limit: '50mb',
  extended: true,
  parameterLimit:50000
}));
app.use('/assets', express.static('assets'));
app.get('/', function (req, res) {
	var path = req.params[0] ? req.params[0] : 'index.html';
   //res.sendFile(path, {root: './assets'});
   res.redirect('/assets/index.html');
});

app.get('/getKeys', function(req, res){
	var keys = new Array();
	for(var i=0; i<localStorage.length;i++){
		keys.push(localStorage.key(i));
	}
	res.write(JSON.stringify({keys: keys}));
	res.end();
});
app.get('/getItem', function(req, res){
	var key = req.query.key;
	//console.log("key: "+key);
	var item = localStorage.getItem(key);
	//console.log(item);
	res.write(item);
	res.end();
});

app.post('/saveItem', function(req, res){
	var key = req.query.key;
	var data = req.query.data;
	//console.log("key: "+key);
	//console.log(data);	
	localStorage.setItem(key, JSON.stringify(data));
	res.write('{"response": "success"}');
	res.end();
});
app.post('/', function(req, res){
	//console.log('Post recieved at http://%s:%s', server_url, server_port);
	//print(request, response);
	//console.log(req.query);
	//console.log(req.params);
	//console.log(request.originalUrl);
	//console.log(req.body);
	console.log(req.body.tag+": "+req.body.message);
		io.sockets.emit('log', {
         tag: req.body.tag,
         message: req.body.message
      });
   res.send({
   	status_code: 1,
   	message: ""
   });	
});
var server = app.listen(server_port, function () {
	  var host = server.address().address;
	  var port = server.address().port;

	  console.log('Example app listening at http://%s:%s', host, port);
});

var MidiOutput = new midi.output();
var MidiPortIsOpen = false;
// Count the available output ports.
console.log(MidiOutput.getPortCount());

// Get the name of a specified output port.
console.log(MidiOutput.getPortName(0));

// Open the first available output port.
MidiPortIsOpen = true;
MidiOutput.openPort(0);

// Send a MIDI message.
if(MidiPortIsOpen)
	MidiOutput.sendMessage([176,22,1]);



io.on('connection', function (socket) {
	console.log("new connection...");

    socket.emit('log', {
        tag: "debug",
        message: "begin session..."
    });
});

process.on('SIGINT', function() {    
	 if(MidiPortIsOpen != null && MidiPortIsOpen){
		MidiOutput.closePort();
	 	console.log("Closing Midi Port...");
	 }
    process.exit();
});
